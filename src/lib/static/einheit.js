export const einheiten = [
	{
		id: 'o',
		titel: 'Warsingsfehn',
		stufe: 'Stützpunktfeuerwehr',
		bild: 'https://static.wixstatic.com/media/6cd577_b3ea59a184dd4a84bb7cea8a4c72041c~mv2.jpg',
		jugendfeuerwehr: true,
		kinderfeuerwehr: false,
		links: [
			{
				label: 'Homepage',
				link: 'http://www.feuerwehr-warsingsfehn.de'
			},
			{
				label: 'Einsatzberichte',
				link: 'https://feuerwehr-warsingsfehn.de/einsaetze-2024'
			},
			{
				label: 'Facebook',
				link: 'https://www.facebook.com/feuerwehrwarsingsfehn'
			},
			{
				label: 'Instagram',
				link: 'https://www.instagram.com/feuerwehr.warsingsfehn/'
			}
		]
	},
	{
		id: 'j',
		titel: 'Neermoor',
		stufe: 'Stützpunktfeuerwehr',
		bild: 'https://static.wixstatic.com/media/6cd577_141a6b5d13cc449da7cc3f93e896633c~mv2_d_2272_1704_s_2.jpg',
		jugendfeuerwehr: true,
		kinderfeuerwehr: true,
		links: [
			{
				label: 'Homepage',
				link: 'https://www.feuerwehr-neermoor.de/'
			},
			{
				label: 'Einsatzberichte',
				link: 'https://www.feuerwehr-neermoor.de/index.php/einsaetze'
			},
			{
				label: 'Facebook',
				link: 'https://www.facebook.com/feuerwehrneermoor'
			},
			{
				label: 'Instagram',
				link: 'https://www.instagram.com/feuerwehr_neermoor/'
			}
		]
	},
	{
		id: 'o',
		titel: 'Oldersum',
		stufe: 'Stützpunktfeuerwehr',
		bild: 'https://static.wixstatic.com/media/6cd577_251e0571f8c940f2a929e977610a6855~mv2_d_4288_2848_s_4_2.jpg',
		jugendfeuerwehr: true,
		kinderfeuerwehr: true,
		links: [
			{
				label: 'Facebook',
				link: 'https://www.facebook.com/oldersum.feuerwehr/'
			},
			{
				label: 'Instagram',
				link: 'https://www.instagram.com/feuerwehroldersum'
			}
		]
	},
	{
		id: 'v',
		titel: 'Veenhusen',
		stufe: 'Ortsfeuerwehr mit Grundausstattung',
		bild: 'https://static.wixstatic.com/media/6cd577_39b0e1d1860042ec8ef6caeaf478d3a9~mv2.jpg',
		jugendfeuerwehr: true,
		kinderfeuerwehr: false,
		links: [
			{
				label: 'Homepage',
				link: 'https://www.feuerwehr-veenhusen.de/'
			},
			{
				label: 'Einsatzberichte',
				link: 'https://veenhusen.einsatzprotokoll.com/'
			},
			{
				label: 'Facebook',
				link: 'https://www.facebook.com/feuerwehr.veenhusen'
			},
			{
				label: 'Instagram',
				link: 'https://www.instagram.com/feuerwehr_veenhusen/'
			}
		]
	},
	{
		id: 'o',
		titel: 'Hatshausen',
		stufe: 'Ortsfeuerwehr mit Grundausstattung',
		bild: 'https://static.wixstatic.com/media/6cd577_b478faf3cf774dfa863826da10836f29~mv2_d_2517_1678_s_2.jpg',
		jugendfeuerwehr: true,
		kinderfeuerwehr: false,
		links: [
			{
				label: 'Facebook',
				link: 'https://www.facebook.com/people/Feuerwehr-Hatshausen/100078668823327/'
			}
		]
	},
	{
		id: 'j',
		titel: 'Jheringsfehn',
		stufe: 'Ortsfeuerwehr mit Grundausstattung',
		bild: 'https://static.wixstatic.com/media/6cd577_9397a96426ee4c49b17189179ae570de~mv2.jpg',
		jugendfeuerwehr: true,
		kinderfeuerwehr: true,
		links: [
			{
				label: 'Homepage',
				link: 'https://www.feuerwehr-jheringsfehn.de/'
			},
			{
				label: 'Facebook',
				link: 'https://www.facebook.com/FreiwilligeFeuerwehrJheringsfehn'
			},
			{
				label: 'Instagram',
				link: 'https://www.instagram.com/feuerwehrjheringsfehn/'
			}
		]
	}
];
