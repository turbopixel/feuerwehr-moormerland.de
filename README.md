# Feuerwehren in der Gemeinde Moormerland

Eine simple Ladingpage mit Übersicht über alle Freiwillige Feuerwehren der Gemeinde Moormerland.

Homepage: [www.feuerwehr-moormerland.de](https://www.feuerwehr-moormerland.de/)

# Entwicklung

Entwickelt wurde das Projekt mit [Svelte](https://github.com/sveltejs/kit/).

### Mitmachen

**Projekt klonen**

Zum Klonen muss [GIT](https://git-scm.com/) installiert sein.

```bash
git clone git@codeberg.org:turbopixel/feuerwehr-moormerland.de.git
```

**Abhängigkeiten installieren**

```bash
npm install
```

**Dev Server starten**

```bash
npm run dev
```

## Projekt packen

Zum kompilieren der Daten wird der Befehl im Terminal ausgeführt:

```bash
npm run build
```
